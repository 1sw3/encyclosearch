package io.gitlab.hasanger.encyclosearch;

import info.debatty.java.stringsimilarity.Levenshtein;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Encyclopedia {
	
	// Levenshtein instance, used for calculating Levenshtein distance (how similar two Strings are)
	public static Levenshtein l = new Levenshtein();
	
	// Various properties
	public String name, imageName, searchUrl, titleSelector, descriptionSelector, urlSelector, textToRemove;
	
	/**
	 * Create a new Encyclopedia.
	 * @param encyclopediaObject A JSONObject containing the required information
	*/
	public Encyclopedia(JSONObject encyclopediaObject) {
		this.name = encyclopediaObject.getString("name");
		this.imageName = encyclopediaObject.getString("imageName");
		this.searchUrl = encyclopediaObject.getString("searchUrl");
		this.titleSelector = encyclopediaObject.getString("titleSelector");
		this.descriptionSelector = encyclopediaObject.getString("descriptionSelector");
		this.urlSelector = encyclopediaObject.getString("urlSelector");
	}
	
	/**
	 * Search this {@link Encyclopedia}.
	 * @param query The keyword to search for
	 * @param timeout The timeout for search requests
	 * @param limit The maximum number of results
	 */
	public ArrayList<ArticleData> search(String query, int timeout, int limit) throws IOException {
		
		// Get the search URL
		String searchUrl = this.searchUrl.replace("QUERY", query);
		
		// Load the document
		Document document = Jsoup.connect(searchUrl).timeout(5 * 1000).get();
		
		// Instantiate SearchResults object
		ArrayList<ArticleData> dataList = new ArrayList<>();
		
		for(int i = 0; i < limit; i++) {
			try {
				
				ArticleData data = new ArticleData();
				
				// Assign all the values we can right now
				data.encyclopediaName = this.name;
				data.encyclopediaImageName = this.imageName;
				data.searchUrl = searchUrl;
				
				// Get article title. If the selector doesn't return anything, the end of the results has been reached.
				String title = clean(document.select(titleSelector).get(i));
				if(title == null) break;
				data.articleTitle = title.trim();
				
				// Get description
				data.description = clean(document.select(descriptionSelector).get(i)).trim();
				
				// Get article URL (not link to search page, direct link to article)
				data.url = clean(document.select(urlSelector).get(i).attr("abs:href")).trim();
				
				
				// Calculate relevance
				
				// Modified article title, used for sorting only
				String articleTitle = data.articleTitle
						.replaceAll("\\([^)]*\\)",""); // Remove parentheses
				
				// Add +2000 to relevance if title is exact match (case sensitive)
				if(articleTitle.equals(query)) data.relevance += 2000;
				
				// Add +1000 to relevance if title is exact match (not case sensitive)
				if(articleTitle.equalsIgnoreCase(query)) data.relevance += 1000;
				
				String[] splitQuery = query.split(" ");
				
				// Add +100 to relevance if title contains all keywords
				boolean containsAll = true;
				for(String keyword : splitQuery) {
					if(!articleTitle.contains(keyword)) {
						containsAll = false;
						break;
					}
				}
				if(containsAll) data.relevance += 100;
				
				// Add +50 to relevance if title contains some keywords
				boolean containsSome = false;
				for(String keyword : splitQuery) {
					if(articleTitle.contains(keyword)) {
						containsSome = true;
						break;
					}
				}
				if(containsSome) data.relevance += 50;
				
				
				// Calculate Levenshtein relevance (how similar the query and the article title are)
				data.levenshteinRelevance = similarity(articleTitle.toLowerCase(), query.toLowerCase());
				
				// Add 1000 to the levenshtein relevance if the title doesn't contain the query (higher = less relevant)
				if(!data.articleTitle.toLowerCase().contains(query.toLowerCase())) data.levenshteinRelevance += 1000;
				
				// Add the data to the list
				if(!dataList.contains(data)) dataList.add(data);
				
			} catch(IndexOutOfBoundsException ioobe) {} // Swallow exception
		}
		
		// Return the list of results
		return dataList;
	}
	
	// Removes HTML tags from the given String/object.
	// If it isn't a String, it will be converted to a String.
	public static String clean(Object obj) {
		return Jsoup.clean(obj.toString(), Whitelist.none());
	}
	
	// Used to calculate Levenshtein distance
	public static int similarity(String one, String two) {
		return (int) l.distance(one, two);
	}
	
	
	
	/**
	 * Encodes the passed String as UTF-8 using an algorithm that's compatible
	 * with JavaScript's <code>encodeURIComponent</code> function. Returns
	 * <code>null</code> if the String is <code>null</code>.
	 *
	 * @param s The String to be encoded
	 * @return the encoded String
	 * @see <a href="https://stackoverflow.com/a/611117/5905216"></a>
	 */
	public static String encodeURIComponent(String s)
	{
		String result;
		
		try
		{
			result = URLEncoder.encode(s, "UTF-8")
					.replaceAll("\\+", "%20")
					.replaceAll("%21", "!")
					.replaceAll("%27", "'")
					.replaceAll("%28", "(")
					.replaceAll("%29", ")")
					.replaceAll("%7E", "~");
		}
		
		// This exception should never occur.
		catch (UnsupportedEncodingException e)
		{
			result = s;
		}
		
		return result;
	}

}
