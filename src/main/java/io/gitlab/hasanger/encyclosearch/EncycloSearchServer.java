package io.gitlab.hasanger.encyclosearch;

import static spark.Spark.*;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;

import org.json.JSONObject;
import org.json.JSONStringer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Service;
import spark.Spark;

/*
 * The EncycloSearchServer class is the main class.
 * It's small, because all it is is a basic server.
 * Most of the code is in the Encyclopedia and Encyclopedias classes.
*/
public class EncycloSearchServer {
	
	// Logger
	static Logger logger = LoggerFactory.getLogger(EncycloSearchServer.class);
	
	// limit: The maximum number of results to retrieve from each encyclopedia
	// timeout: The timeout for search requests
	static int limit = 5, timeout = 3;
	
	// cacheSize: The maximum allowed cache size.
	static int cacheSizeLimit = 50;
	
	// Cache of SearchResults
	public static ArrayList<SearchResults> cache = new ArrayList<>();


	// Path to the keystore file, which contains the SSL certificate
	public static String keystoreFile = "";

	// The keystore password
	public static String keystorePassword = "";
	
	public static void main(String[] args) {
		
		// Get configuration
		String ipAddress = "0.0.0.0";
		int port = 8080, httpPort = 80;
		boolean enableSsl = false, enableHttpToHttpsRedirect = false;
		String redirectUrl = "";
		try {
			JSONObject config = new JSONObject(new String(Files.readAllBytes(new File("config.json").toPath())));
			ipAddress = config.getString("ipAddress");
			port = config.getInt("port");
			limit = config.getInt("resultsLimit");
			timeout = config.getInt("timeout");
			cacheSizeLimit = config.getInt("cacheSizeLimit");

			enableSsl = config.getBoolean("enableSsl");
			keystoreFile = config.getString("keystoreFile");
			keystorePassword = config.getString("keystorePassword");
			enableHttpToHttpsRedirect = config.getBoolean("enableHttpToHttpsRedirect");
			httpPort = config.getInt("httpPort");
			redirectUrl = config.getString("redirectUrl");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// If enabled, set up redirect server
		if(enableHttpToHttpsRedirect) {
			Service httpServer = Service.ignite();
			httpServer.ipAddress(ipAddress);
			httpServer.port(httpPort);
			String finalRedirectUrl = redirectUrl;
			httpServer.get("*", (req, res) -> { res.redirect(finalRedirectUrl, 301); return null; });
		}
		
		// Set up server
		
		// Set IP address and port based on config.json
		ipAddress(ipAddress);
		port(port);

		// Enable SSL if necessary
		if(enableSsl) secure(keystoreFile, keystorePassword, null, null);
		
		// Set up static files
		staticFiles.externalLocation(System.getProperty("user.dir") + "/src/main/resources/public");
		
		
		// GET request for search
		get("/api/search/:query/:enableCache", (req, res) -> {
			
			logger.info("Searching for \"" + req.params("query") + "\"..."); // Log message
			
			res.type("application/json"); // Let the browser know that this is JSON
			
			// Instantiate JSONStringer for results
			JSONStringer stringer = new JSONStringer();
			stringer.object()
					.key("searchResults").array();
			
			// The SearchResults object
			SearchResults results = null;
			
			// Only return cached results if cache is enabled
			if(Boolean.parseBoolean(req.params("enableCache"))) {
				// Check the cache of SearchResults for cached results for the given query.
				// If a cached version exists, then use that.
				boolean gotCachedResults = false;
				for(SearchResults cachedResults : cache) {
					if(cachedResults.query.equals(req.params("query"))) {
						results = cachedResults;
						gotCachedResults = true;
					}
				}
				
				// If cached results could not be found, then generate new ones.
				// Add them to the cache, and remove the last item from the cache.
				if(!gotCachedResults) {
					results = Encyclopedias.searchAll(req.params("query"), timeout, limit); // Get the results
					cache.add(results); // Add them to the cache
					
					// If the cache has reached its maximum allowed size (cacheSize),
					// then remove the oldest item from the cache.
					if(cache.size() >= cacheSizeLimit) cache.remove(0);
				} else {
					logger.info("Page loaded from cache");
				}
			} else {
				results = Encyclopedias.searchAll(req.params("query"), timeout, limit); // Get results, but don't cache them
			}
		
			// Add the results to the JSON object
			for(ArticleData data : results.results) {
				stringer.object()
						.key("encyclopediaName").value(data.encyclopediaName)
						.key("encyclopediaImageName").value(data.encyclopediaImageName)
						.key("articleTitle").value(data.articleTitle)
						.key("description").value(data.description)
						.key("searchUrl").value(data.searchUrl)
						.key("url").value(data.url)
						.endObject();
			}
			stringer.endArray()
					.endObject();
			
			// Return the results
			return stringer.toString();
		});
		
	}

}