package io.gitlab.hasanger.encyclosearch;

public class ArticleData {

	public String encyclopediaName = "", encyclopediaImageName = "", articleTitle = "", description = "", url = "", searchUrl = "";
	
	public int relevance, levenshteinRelevance;
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof ArticleData) return (articleTitle.equals(((ArticleData) other).articleTitle));
		else return false;
	}

}
