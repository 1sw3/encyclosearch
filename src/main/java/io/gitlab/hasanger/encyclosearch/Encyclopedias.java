package io.gitlab.hasanger.encyclosearch;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.CountDownLatch;

public class Encyclopedias {
	
	// Logger for the class
	static Logger logger = LoggerFactory.getLogger(Encyclopedias.class);
	
	// List of encyclopedias
	private static final ArrayList<Encyclopedia> encyclopedias = new ArrayList<>();
	
	// Static initializer. Loads all encyclopedias and puts them in the encyclopedias array
	static {
		try {
			// Load the file
			JSONObject encyclopediasObject = new JSONObject(new String(Files.readAllBytes(new File("encyclopedias.json").toPath())));
			
			// Get the JSONArray of encyclopedias and convert it to an ArrayList
			JSONArray encyclopediasArray = encyclopediasObject.getJSONArray("encyclopedias");
			for(int i = 0; i < encyclopediasArray.length(); i++) {
				encyclopedias.add(new Encyclopedia((JSONObject) encyclopediasArray.get(i)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Searches all encyclopedias.
	 * @param query The text to search for
	 * @param timeout The timeout for search requests
	 * @param limit The maximum number of results to be retrieved from each encyclopedia
	 */
	public static SearchResults searchAll(String query, int timeout, int limit) {
		
		long searchStart = System.currentTimeMillis();
		
		// List of articles to return
		ArrayList<ArticleData> dataList = new ArrayList<>();
		
		// Make a list of threads, so we can block until all threads are done executing
		ArrayList<Thread> threads = new ArrayList<>();
		
		CountDownLatch threadLatch = new CountDownLatch(encyclopedias.size());
		
		
		// Loop over all encyclopedias
		for(Encyclopedia encyclopedia : encyclopedias) {
			
			// Create a new thread for every encyclopedia, querying each simultaneously to save time
			Thread t = new Thread(() -> {
				
				long start = System.currentTimeMillis();
				logger.info("Loading " + encyclopedia.name + " page...");
				
				
				// Scrape the website and add the results to the array
				try {
					ArrayList<ArticleData> data = encyclopedia.search(query, timeout, limit);
					if(data != null) dataList.addAll(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				long end = System.currentTimeMillis();
				float sec = (end - start) / 1000F;
				logger.info("Done loading " + encyclopedia.name + " page. Took " + sec + "s");
				
				threadLatch.countDown();
			});
			threads.add(t); // Add it to the array
		}
		
		
		// Block until all threads are finished executing,
		// so that the array isn't sorted and returned before it's done
		for(Thread t : threads) t.start();
		try {
			threadLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		// Sort data twice: once by relevance (see Encyclopedia class),
		// and once by Levenshtein distance (how similar the article titles are to the query)
		
		// First sort: by Levenshtein distance
		dataList.sort(Comparator.comparingInt(a -> a.levenshteinRelevance));
		
		// Second sort: by relevance
		dataList.sort((a1, a2) -> a2.relevance - a1.relevance);
		
		long searchEnd = System.currentTimeMillis();
		float sec = (searchEnd - searchStart) / 1000F;
		logger.info("Finished search for \"" + query + "\". Took " + sec + "s"); // Log
		
		// Return the results
		return new SearchResults(query, dataList);
	}

}
