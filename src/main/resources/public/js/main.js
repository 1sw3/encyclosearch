$(document).ready(function() {
	$("#resultsPane").hide();

	// Trigger button click when Enter is pressed in the search field
	$('#searchField').keypress(function(e){
    	if(e.keyCode == 13) {
      		$('#searchBtn').click();
     	}
    });

	// Redirect to search.html when search button is clicked
	$("#searchBtn").click(function() {

		var exactPhrase = $("#exactPhrase").prop("checked");
		var cache = !$("#cacheDisable").prop("checked");
		var query = $("#searchField").val();
		query = exactPhrase ? '"' + query + '"' : query;

		if(!(query == null || query.trim() == "")) { // Blank searches don't do anything

			// Show and hide different elements
			$("#resultsPane").show();
			$("#preloader").show();
			$("#resultsCard").hide();
			$("#resultsBox").hide().html(""); // Hide and clear results box

			// Make a GET request to /search
			var start = performance.now();
			$.get("/api/search/" + query + "/" + cache, function(results) {
				console.log(results.searchResults);
				var foundResult = false;
				for(var i = 0; i < results.searchResults.length; i++) {
					foundResult = true;
					var result = results.searchResults[i];
					var newItem = $('<div class="col card mb-3 search-result">'
								  + '	<div class="row g-0">'
							      + '		<div class="card-body">'
							      + '			<div class="encyclopedia-logo-container"><a href="' + result.searchUrl + '"><img class="encyclopedia-logo" src="img/' + result.encyclopediaImageName + '.png" alt="' + result.encyclopediaName + '" draggable="false"></a></div>'
							      + '			<h5 class="card-title"><a href="' + result.url + '">' + result.articleTitle + '</a></h5>'
							      + '			<p class="card-description">' + result.description + '</p>'
							      + '		</div>'
							      + '	</div>'
								  + '</div>');

					$("#resultsBox").append(newItem);
				}
				$("#preloader").hide();
				if(!foundResult) {
					$("#resultsText").html('No results for "' + query + '"');
				} else {
					var end = performance.now();
					$("#resultsText").html(results.searchResults.length + " results (" + ((end - start) / 1000).toFixed(2) + " seconds)");
					$("#resultsCard").show();
					$("#resultsBox").show();
				}
			});
		} else {
			$("#resultsText").css("color", "#dc3545");
			$("#resultsText").html("Search can't be blank.");
		}
	});
});