$(document).ready(function() {
	$(".dropdown-trigger").dropdown({coverTrigger: false}); // Initialize dropdown
	$(".collapsible").collapsible();
	var searchParams = new URLSearchParams(window.location.search);

	// Trigger button click when Enter is pressed in the search field
	$('#searchBox').keypress(function(e){
    	if(e.keyCode == 13) {
      		$('#searchBtn').click();
     	}
    });

	// Redirect to search.html when search button is clicked
	$("#searchBtn").click(function() {
		var exactPhrase = $("#exactPhrase").prop("checked");
		var cache = !$("#cacheDisable").prop("checked");
		var query = $("#searchBox").val();
		window.location.href = "/search.html?q=" + (exactPhrase ? '"' + query + '"' : query) + "&cache=" + cache;
	});
});