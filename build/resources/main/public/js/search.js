$(document).ready(function() {
	$(".collection, .wrapper-wrapper").hide(); // Hide preloader and results box

	// Makes the Enter key start the search
	$('#searchBox').keypress(function(e) {
	    if(e.keyCode == 13) {
	    	window.location.href = "/search.html?q=" + $("#searchBox").val();
	    }
    });

    // Search for given query
    var searchParams = new URLSearchParams(window.location.search);

	// Get cache parameter. Make sure it's always either true or false
    var cache = searchParams.get("cache");
    if(cache != "true") cache = "false";

    var query = searchParams.get("q");
	if(!(query == null || query.trim() == "")) { // Blank searches don't do anything

		// Show and hide different elements
		$(".wrapper-wrapper").show(); // Show preloader
		$(".collection").hide().html(""); // Hide and clear results box

		// Make a GET request to /search
		var start = performance.now();
		$.get("/api/search/" + query + "/" + cache, function(results) {
			console.log(results.searchResults);
			var foundResult = false;
			for(var i = 0; i < results.searchResults.length; i++) {
				foundResult = true;
				var result = results.searchResults[i];
				newItem = $('<a class="collection-item avatar" href="' + result.url + '" target="_blank" />');
				newItem.append($('<img src="img/' + result.encyclopediaImageName + '.png" alt="' + result.encyclopediaName + '" class="circle encyclopedia-logo">'));
				newItem.append('<a class="title" target="_blank" class="truncate"><strong>' + result.articleTitle + "</strong> - " + result.encyclopediaName + '</a>');
				newItem.append('<p>' + result.description + '</p>');
				newItem.append('<a href="' + result.searchUrl + '" class="secondary-content valign-wrapper" target="_blank"><i class="material-icons right">open_in_new</i></a>');
				$(".collection").append(newItem);
			}
			$(".wrapper-wrapper").hide();
			if(!foundResult) {
				$("#resultsText").html('No results for "' + query + '"');
			} else {
				var end = performance.now();
				$("#resultsText").html(results.searchResults.length + ' results for "' + query + '" (' + ((end - start) / 1000).toFixed(2) + ' seconds)');
				$(".collection").show();
			}
		});
	} else {
		$("#resultsText").css("color", "#dc3545");
		$("#resultsText").html("Search can't be blank.");
	}
});
