For upcoming versions, features that have been added are crossed out.

# 1.1 (upcoming)
- Added categories: you can now filter results by category. Categories are: General, Science & Technology, Humanities, Politics & Government, and Other
- Improved design
- Dark theme
- Searching now redirects to `/search?q=QUERY`, so your search is saved
- Added these encyclopedias: Columbia Encyclopedia, Encyclopedia Mythica, Encyclopedia of Life, Encyclopedia of Philosophy, How Stuff Works, Medline Encyclopedia, Ancient History Encyclopedia


# 1.0 (current)
- Basic search functionality
- Added these encyclopedias: Wikipedia, Britannica, Encyclopedia.com, Stanford Encyclopedia of Philosophy, Ballotpedia, HandWiki